import "module-alias/register";
import "reflect-metadata";

import { config } from "dotenv";
if (process.env.NODE_EMV !== "production") config();
import fastify from "fastify";
import multer from "fastify-multer";
import fastifyCors from "fastify-cors";
import AccountRouter from "@routes/AccountRouter";
import TinRouter from "@routes/TinRouter";
import AdminRouter from "@routes/AdminRouter";
import FileRouter from "@routes/FileRouter";
import XanthicRouter from "./routes/XanthicRouter";
import getFolderSize from "@utils/getFolderSize";
import { isMultipart } from 'fastify-multer/src/lib/content-parser'
import { File as file, FilesObject } from 'fastify-multer/src/interfaces'
type FilesInRequest = FilesObject | Partial<File>[]

import { createConnection } from "typeorm";
import { Account } from "@database/Account";
import { Token } from "@database/Token";
import { Invite } from "@database/Invite";
import { File } from "@database/File";
import { Domain } from "@database/Domain";
import { Voucher } from "@database/Voucher";

const app = fastify();
app.register(fastifyCors);
app.register(multer.contentParser);

app.register(AccountRouter, { prefix: "/account" });
app.register(TinRouter, { prefix: "/tin" });
app.register(AdminRouter, { prefix: "/admin" });
app.register(FileRouter, { prefix: "/files" });
app.register(XanthicRouter, { prefix: "/xanthic" });


declare module "fastify" {
    interface FastifyRequest {
        account: Account,
        isMultipart: typeof isMultipart
        file: file
        files: FilesInRequest
    }
};

app.setErrorHandler((err, req, res) => {
    if (err.validation) return res.status(400).send({
        success: false,
        error: err.validation[0].message
    });

    if (!err.statusCode) console.error(err);

    res.status(err.statusCode || 500).send({
        success: false,
        error: err.statusCode && err.message || "internal server error"
    });
});

app.setNotFoundHandler(async (req, res) => {
    res.status(404).send({
        success: false,
        error: "invaild api route"
    });
});

app.get("/domains", async () => {
    const domains = await Domain.find({ });
    
    return {
        success: true,
        domains
    }
});

app.get("/accounts", async (req, res) => {
    return {
        success: true,
        accounts: await Account.count({ })
    };
});

app.get("/files", async (req, res) => {
    return {
        success: true,
        files: await File.count({ }),
        size: await getFolderSize(process.env.FILE_PATH)
    };
});

createConnection({
    type: "postgres",
    username: process.env.PG_USERNAME,
    password: process.env.PG_PASSWORD,
    database: process.env.PG_DB,
    host: process.env.PG_HOST,
    entities: [ Account, Token, Invite, File, Domain, Voucher ],
}).then(async () => {
    app.listen(process.env.PORT, () => console.log("HTTP is up"));
});