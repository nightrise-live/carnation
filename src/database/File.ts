import { Entity, PrimaryGeneratedColumn, BaseEntity, Column, OneToOne, JoinColumn } from "typeorm";
import { Account } from "./Account";

@Entity()
export class File extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    path: string;

    @Column({ type: "simple-json", nullable: true, default: { } })
    metaData: any;

    @OneToOne(() => Account)
    @JoinColumn()
    uploader: Account;

    @Column()
    ContentType: string;

    @Column()
    accessKey: string;

    @Column({ default: true })
    accessible: boolean;

    toObject() {
        let data = { ...this }
        if (data.uploader) data.uploader = data.uploader.toObject();
        delete data.path;
        
        return data;
    };
};