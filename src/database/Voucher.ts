import { Entity, PrimaryGeneratedColumn, BaseEntity, Column, JoinColumn, OneToOne } from "typeorm";
import generate from "@utils/TulipGenerator";

@Entity()
export class Voucher extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @Column()
    type: number;

    @Column({
        default: false
    })
    redeemed: boolean;

    @Column()
    value: string;

    static async generateVoucher(type: number, value: string) {
        const code = generate(2);

        const voucher = new this();
        voucher.code = code;
        voucher.type = type;
        voucher.value = value;

        return await voucher.save();
    };
};