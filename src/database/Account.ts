import { Entity, PrimaryGeneratedColumn, BaseEntity, Column } from "typeorm";
import { hash } from "argon2";

@Entity()
export class Account extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    passwordHash: string;

    @Column({
        default: 0
    })
    invites: number;

    @Column({
        default: true
    })
    enabled: boolean;

    @Column()
    reason: string;

    @Column({
        type: "simple-json",
        default: { }
    })
    settings: any;

    @Column()
    discordId: string;

    @Column({ default: new Date() })
    createdOn: Date;

    @Column({
        default: 0
    })
    storageUsed: number;

    @Column()
    storageLimit: number;

    static async register(name: string, password: string) {
        if (await this.findOne({ name })) throw new Error("name is already used");

        const account = new this();
        account.name = name;
        account.passwordHash = await hash(password);

        return await account.save();
    };

    toObject() {
        const data = { ...this };
        
        delete data.passwordHash;
        if (!data.reason) delete data.reason;
        if (data.discordId === null) delete data.discordId;

        return {
            ...data,
            createdOn: data.createdOn.toLocaleString()
        };
    };
};