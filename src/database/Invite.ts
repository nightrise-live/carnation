import { Entity, PrimaryGeneratedColumn, BaseEntity, Column, JoinColumn, OneToOne } from "typeorm";
import { Account } from "./Account";
import generate from "@utils/TulipGenerator";

@Entity()
export class Invite extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @Column({
        default: false
    })
    used: boolean;

    @OneToOne(() => Account)
    @JoinColumn()
    generator: Account;

    static async generateInvite(generator?: Account) {
        const invite = new Invite();
        invite.code = generate(1);

        if (generator) invite.generator = generator;

        return await invite.save();
    };
};