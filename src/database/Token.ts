import { Entity, PrimaryGeneratedColumn, BaseEntity, Column, JoinColumn, OneToOne } from "typeorm";
import { Account } from "./Account";
import { randomBytes } from "crypto";
import TokenOptions from "@utils/TokenOptions";

@Entity()
export class Token extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    token: string;

    @Column({ default: 0 })
    type: number;

    @OneToOne(() => Account)
    @JoinColumn()
    account: Account;
 
    static async generateToken(options: TokenOptions) {
        const token = new this();

        token.token = randomBytes(30).toString("base64");
        token.account = options.account;
        token.type = options.type || 0;
        
        return await token.save();
    };
};