import { verify } from "argon2";
import { FastifyInstance } from "fastify"

import TinAuth from "@utils/middlewares/TinAuth";
import CredentialSchema from "@schemas/CredentialSchema";
import TokenSchema from "@schemas/TokenSchema";
import TokenFetchSchema from "@schemas/TokenFetchSchema";

import { Account } from "@database/Account";
import { Token } from "@database/Token";

export default async (router: FastifyInstance) => {
    router.post("/create", {
        schema: { body: TokenSchema },
        preHandler: TinAuth()
    }, async (req) => {
        const user: Account = req.account;
        const { body }: { body: any } = req;

        const { token } = await Token.generateToken({
            account: user,
            type: body.type
        })
    
        return {
            sucesss: true,
            token
        };
    });
    
    router.post("/create/credentials", {
        schema: { body: CredentialSchema }
    }, async (req, res) => {
        const { name, password } = req.body as any;
    
        const account = await Account.findOne({ name });

        if (!account || !await verify(account.passwordHash, password)) return res.status(406).send({
            success: false,
            error: "invaild credentials"
        });
        
        if (!account.enabled) return res.status(406).send({
            success: false,
            error: "your account has been disabled for " + (account.reason || "no reason specified")
        });

        const { token } = await Token.generateToken({
            account,
        });
    
        return {
            success: true,
            token
        };
    });

    router.get("/token", {
        preHandler: TinAuth()
    }, async (req, res) => {
        const user: Account = req.account;

        return {
            success: true,
            tokens: await Token.find({ account: user })
        };
    });

    router.delete("/token", {
        preHandler: TinAuth(),
        schema: { body: TokenFetchSchema }
    }, async (req, res) => {
        const user: Account = req.account;
        const { body }: { body: any } = req;

        const token = await Token.findOne({ token: body.token, account: user });
        if (!token) return res.status(404).send({
            success: false,
            error: "token was not found"
        });

        await token.remove();

        return {
            success: true,
            token
        };
    });
};