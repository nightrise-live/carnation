import { FastifyInstance } from "fastify"
import { Invite } from "@database/Invite";
import { Account } from "@database/Account";
import { Domain } from "@database/Domain";
import { Token } from "@database/Token";

import { File } from "@database/File";
import { hash } from "argon2";
import { unlink } from "fs/promises";
import { createClient } from "redis";
import {
    AccountDiscordSchema,
    AccountNameSchema,
    AccountToggleSchema,
    VoucherCreationSchema,
    WaveSchema,
    InviteSchema,
    NotificationSchema,
    TokenSchema,
    TokenFetchSchema,
    PasswordSchema,
    DomainSchema
} from "@schemas/index";

import AccountId from "@utils/middlewares/AccountId";
import { Voucher } from "@database/Voucher";

const client = process.env.USE_REDIS ? createClient() : { publish: () => {} }; // laughable code here

export default async (router: FastifyInstance) => {
    router.addHook("preHandler", async (req, res, next) => {
        const { headers } = req;

        if (headers.authorization !== process.env.ADMIN_KEY) return res.status(401).send({
            success: false,
            error: "unauthorized"
        });
    });
    
    router.post("/invite/create", async () => {
        const invite = await Invite.generateInvite();

        return {
            success: true,
            code: invite.code
        };
    });

    router.post("/voucher/create", {
        schema: { body: VoucherCreationSchema }
    }, async (req) => {
        const { body }: { body: any } = req;

        const voucher = await Voucher.generateVoucher(body.type, body.value);

        return {
            success: true,
            code: voucher.code
        };
    });

    router.put("/invite/wave", {
        schema: { body: WaveSchema }
    }, async (req, res) => {
        const { body }: { body: any } = req;

        await Account.getRepository().increment({ }, "invites", body.invite);

        return { 
            success: true,
            invites: body.invite
        };
    });

    router.get("/invite", {
        schema: {
            querystring: InviteSchema
        }
    }, async (req, res) => {
        const { query }: { query: any } = req;

        const invite = await Invite.findOne({
            code: query.invite
        }, { relations: ["generator"] });

        if (!invite) return res.status(404).send({
            success: false,
            error: "invite was not found"
        });

        if (invite.generator) invite.generator = invite.generator.toObject();

        return {
            success: true,
            invite
        };
    });

    router.delete("/invite", {
        schema: {
            querystring: InviteSchema
        }
    }, async (req, res) => {
        const { query }: { query: any } = req;

        const invite = await Invite.findOne({
            code: query.invite
        });

        if (!invite) return res.status(404).send({
            success: false,
            error: "invite was not found"
        });

        await invite.remove();

        return {
            success: true,
            invite
        };
    })

    router.get("/account", {
        schema: { querystring: AccountNameSchema }
    }, async (req, res) => {
        const { query }: { query: any } = req;

        const account = await Account.findOne({
            name: query.name
        });

        if (!account) res.status(404).send({
            success: false,
            error: "account was not found"
        });

        return {
            success: true,
            account: account.toObject()
        };
    });

    router.get("/account/discord", {
        schema: { querystring: AccountDiscordSchema }
    }, async (req, res) => {
        const { query }: { query: any } = req;

        const account = await Account.findOne({
            discordId: query.id
        });

        if (!account) res.status(404).send({
            success: false,
            error: "account was not found"
        });

        return {
            success: true,
            account: account.toObject()
        };
    });

    router.get("/account/:id", {
        preHandler: AccountId
    }, async (req, res) => {
        const { body }: { body: any } = req;
        const account: Account = req.account;

        return {
            success: true,
            account: account.toObject()
        };
    });

    router.patch("/account/:id/toggle", { 
        schema: { body: AccountToggleSchema },
        preHandler: AccountId
    }, async (req, res) => {
        const { body }: { body: any } = req;
        const account: Account = req.account;
        
        if (account.discordId && account.enabled) {
            client.publish("xanthic", JSON.stringify({
                type: 0,
                user: account.discordId,
                message: `your account has been disabled for ${body.reason ? body.reason : "no reason specified"}`
            }));

            client.publish("xanthic", JSON.stringify({
                type: 1,
                user: account.discordId
            }));

            account.discordId = null;
        };
        
        account.reason = body.reason;
        account.enabled = body.enabled;

        await account.save();


        return {
            success: true,
            account: account.toObject()
        };
    });

    router.put("/account/:id/invites", {
        schema: { body: WaveSchema },
        preHandler: AccountId
    }, async (req, res) => {
        const account: Account = req.account;
        const { body }: { body: any } = req;
        
        account.invites += body.invite;

        return { 
            success: true,
            invites: body.invite
        };
    });

    router.post("/account/:id/unlink", {
        preHandler: AccountId
    }, async (req, res) => {
        const account: Account = req.account;
        const { body }: { body: any } = req;

        if (!account.discordId) return res.status(409).send({
            success: false,
            error: "account is not linked"
        });
        
        client.publish("xanthic", JSON.stringify({
            type: 1,
            user: account.discordId
        }));

        account.discordId = null;
        await account.save();

        return { 
            success: true,
            message: "unlinked account"
        };
    });

    router.post("/account/:id/notify", {
        preHandler: AccountId,
        schema: { body: NotificationSchema }
    }, async (req, res) => {
        const account: Account = req.account;
        const { body }: { body: any } = req;

        if (!account.discordId) return res.status(409).send({
            success: false,
            error: "account is not linked"
        });
        
        client.publish("xanthic", JSON.stringify({
            type: 0,
            user: account.discordId,
            message: body.message
        }));

        return { 
            success: true,
            message: "notified the account"
        };
    });

    router.post("/account/:id/token/create", {
        preHandler: AccountId,
        schema: { body: TokenSchema }
    }, async (req, res) => {
        const user: Account = req.account;
        const { body }: { body: any } = req;

        const { token } = await Token.generateToken({
            account: user,
            type: body.type
        })
    
        return {
            sucesss: true,
            token
        };
    });

    router.get("/account/:id/token", {
        preHandler: AccountId
    }, async (req, res) => {
        const user: Account = req.account;

        return {
            success: true,
            tokens: await Token.find({ account: user })
        };
    });

    router.delete("/account/:id/token", {
        preHandler: AccountId,
        schema: { body: TokenFetchSchema }
    }, async (req, res) => {
        const user: Account = req.account;
       const { body }: { body: any } = req;

        const token = await Token.findOne({ token: body.token, account: user });
        if (!token) return res.status(404).send({
            success: false,
            error: "token was unable to be found"
        });

        await token.remove();

        return {
            success: true,
            token
        };
    });

    router.get("/account/:id/invites", {
        preHandler: AccountId
    }, async (req, res) => {
        const account: Account = req.account;
        const { body }: { body: any } = req;

        return { 
            success: true,
            invites: await Invite.find({ generator: account })
        };
    });

    router.patch("/account/:id/password", {
        preHandler: AccountId,
        schema: { body: PasswordSchema }
    }, async (req, res) => {
        const { body }: { body: any } = req;
        const account: Account = req.account;
        account.passwordHash = await hash(body.password);
        await account.save();
        
        return {
            success: true,
            account: account.toObject()
        };
    });

    router.put("/domain", {
        schema: { body: DomainSchema }
    }, async (req, res) => {
       const { body }: { body: any } = req;

        if (await Domain.findOne({ name: body.name })) return res.status(409).send({
            success: false,
            error: "domain is already added"
        });

        const domain = new Domain();
        domain.name = body.name;

        if (body.hasOwnProperty("wildcard")) domain.wildcard = body.wildcard;

        await domain.save();

        return {
            success: true,
            domain
        };
    });

    router.delete("/domain", {
        schema: { body: DomainSchema }
    }, async (req, res) => {
       const { body }: { body: any } = req;
        const domain = await Domain.findOne({ name: body.name });

        if (!domain) return res.status(404).send({
            success: false,
            error: "domain is invaild"
        });

        await domain.remove();

        return {
            success: true,
            domain
        };
    }); 

    router.delete("/files/:filename", async (req, res) => {
        const { params }: { params: any } = req;

        const file = await File.findOne({ name: params.filename, accessible: true });
        if (!file) return res.status(404).send({
            success: false,
            error: "file was not found"
        });

        file.accessible = false;
        await file.save();

        return { 
            success: true,
            message: "deleted file"
        };
    });
};