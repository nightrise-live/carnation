import { FastifyInstance } from "fastify"
import { Account } from "@database/Account";
import TinAuth from "@utils/middlewares/TinAuth";
import { Invite } from "@database/Invite"
import { Domain } from "@database/Domain";
import { hash } from "argon2"
import { Voucher } from "@database/Voucher";
import { File } from "@database/index";
import { 
    RegisterSchema,
    VoucherSchema,
    AccountSettingsSchema,
    PasswordSchema
} from "@schemas/index";

export default async (router: FastifyInstance, opts: any, done: any) => {
    router.post("/register", {
        schema: { body: RegisterSchema }
    }, async (req, res) => {
        const { name, password, invite } = <any>req.body;

        const invNode = await Invite.findOne({
            code: invite
        });

        if (!invNode || invNode.used) return res.status(406).send({
            success: false,
            error: "invaild invite"
        });

        try {
            const account = await Account.register(name, password);

            invNode.used = true;
            await invNode.save();
            
            return {
                success: true,
                account: account.id
            };
        } catch(e) {
            res.status(409).send({
                success: false,
                error: e.message
            });
        };
    });
    
    router.get("/@me", {
        preHandler: TinAuth()
    }, async (req, res) => {
        const user = req.account;
        
        return {
            success: true,
            user: user.toObject()
        };
    });

    router.post("/@me/voucher/redeem", {
        preHandler: TinAuth(),
        schema: { body: VoucherSchema }
    }, async (req, res) => {
        const { body }: { body: any } = req;
        const account: Account = req.account;
        const voucher = await Voucher.findOne({ code: body.code });

        if (!voucher || voucher.redeemed) return res.status(404).send({
            success: false,
            error: "invaild voucher or used"
        });

        if (voucher.type === 0) {
            const value = parseFloat(voucher.value);

            account.storageLimit += value;
        };

        voucher.redeemed = true

        await Promise.all([
            voucher.save(),
            account.save()
        ]);

        return {
            success: true,
            type: voucher.type,
            value: voucher.value
        };
    });

    router.post("/@me/invite/create", {
        preHandler: TinAuth()
    }, async (req, res) => {
        const account: Account = req.account;

        if (account.invites < 1) return res.status(403).send({
            success: false,
            error: "you do not have enough invites to generate"
        });

        account.invites--;
        await account.save();

        const invite = await Invite.generateInvite(account);

        return {
            success: true,
            invite: invite.code
        };
    });

    router.patch("/@me/settings", {
        schema: { body: AccountSettingsSchema },
        preHandler: TinAuth()
    }, async (req, res) => {
        const { body }: { body: any } = req;
        const account: Account = req.account;
        account.settings = account.settings || {};

        if (body.domain) {
            const domain = await Domain.findOne({ name: body.domain });
            if (!domain) return res.status(404).send({
                success: false,
                error: "domain is invaild"
            });

            const subDomain = domain.wildcard && body.subDomain ? body.subDomain + "." : "";
            account.settings.domain = subDomain + body.domain;
        };  

        if (body.hasOwnProperty("showUrl")) account.settings.showUrl = body.showUrl;
        if (body.hasOwnProperty("fileNameLength")) account.settings.fileNameLength = body.fileNameLength;
        if (body.hasOwnProperty("path")) account.settings.path = body.path.join("/");
        if (body.hasOwnProperty("noExtension")) account.settings.noExtension = body.noExtension;

        await account.save();

        return {
            success: true,
            account: account.toObject()
        };
    });

    router.get("/@me/invites", {
        preHandler: TinAuth()
    }, async (req, res) => {
        const account: Account = req.account;

        return {
            success: true,
            invites: await Invite.find({ generator: account }) 
        };
    });


    router.delete("/@me/files/:filename", {
        preHandler: TinAuth()
    }, async (req, res) => {
        const { params }: { params: any } = req;

        const file = await File.findOne({ 
            uploader: req.account, 
            accessible: true,
            name: params.filename
        });

        if (!file) return res.status(404).send({
            success: false,
            error: "invaild file"
        });

        file.accessible = false;
        await file.save();
        
        return { 
            success: true,
            message: "deleted file"
        };
    });

    router.patch("/@me/password", {
        preHandler: TinAuth(),
        schema: { body: PasswordSchema }
    }, async (req, res) => {
        const { body }: { body: any } = req;

        const account: Account = req.account;
        account.passwordHash = await hash(body.password);
        await account.save();

        return {
            success: true,
            account: account.toObject()
        };
    });
};
