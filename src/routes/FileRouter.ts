import { FastifyInstance } from "fastify";
import TinAuth from "@utils/middlewares/TinAuth";
import { TokenType } from "@utils/interfaces/TokenType";
import { randomBytes } from "crypto";
import { writeFile, unlink } from "fs/promises";
import { extname } from "path";
import { File } from "@database/File";
import FileDeletionSchema from "@schemas/FileDeletionSchema";
import { generate } from "randomstring";
import multer from "fastify-multer";
import { join } from "path";
import SendWebhook from "@utils/DiscordWebhook";
import FileName from "@utils/FileName";

const upload = multer({ storage: multer.memoryStorage() });

export default async (router: FastifyInstance) => {
    router.post("/upload", {
        preHandler: [ 
            TinAuth({ type: TokenType.UPLOAD }),
            upload.single("file")
        ]
    }, async (req, res) => {
        const protocol = process.env.USE_HTTPS ? "https" : "http";
        const { file } = req;
        const account = req.account;
        const { settings } = account;

        if (!file) return res.status(400).send({
            success: false,
            error: "invaild field"
        });

        const size = file.size / (1000 * 1000);

        if (!settings || !settings.domain) res.status(400).send({
            success: false,
            error: "domain is not specified"
        });

        const name = generate({
            charset: "alphanumeric",
            length: settings.fileNameLength ? settings.fileNameLength : 8
        });

        account.storageUsed += size;
        if (account.storageUsed >= account.storageLimit) return res.status(401).send({
            success: false,
            error: "all account storage capacity has been used"
        });
        
        const nameCtx = new FileName(name, extname(file.originalname));
        const fileName = settings.noExtension ? nameCtx.getWithout() : nameCtx.getWith();

        const path = join(process.env.FILE_PATH , nameCtx.getWith());
        const accessKey = randomBytes(15).toString("hex");
        const urlPath = (settings.path ? settings.path + "/" : "") + fileName;
        const url = `${settings.showUrl ? "\u200b" : ""}${protocol}://${settings.domain}/${urlPath}`;

        await Promise.all([ 
            File.create({
                path,
                name: fileName,
                ContentType: file.mimetype,
                uploader: account,
                accessKey
            }).save(),
            writeFile(path, file.buffer),
            account.save()
        ]);

        if (process.env.LOG_WEBHOOK) SendWebhook(process.env.LOG_WEBHOOK, url);

        return {
            success: true,
            url,
            deletionUrl: `${process.env.BACKEND_URL}/files/delete?k=${accessKey}`,
            accessKey
        };
    });

    router.get("/delete", {
        schema: { querystring: FileDeletionSchema }
    }, async (req, res) => {
        const { query }: { query: any } = req;

        const file = await File.findOne({ accessKey: query.k, accessible: true });

        if (!file) return res.status(404).send({
            success: false,
            error: "invaild file"
        });

        file.accessible = false;
        await file.save();

        return { 
            success: true,
            message: "deleted file"
        };
    });
};