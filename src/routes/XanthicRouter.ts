import { FastifyInstance } from "fastify"
import { Invite } from "@database/Invite";
import { Account } from "@database/Account";
import { Domain } from "@database/Domain";
import { Token } from "@database/Token";
import Xanthic from "@utils/middlewares/Xanthic";

import TokenFetchSchema from "@schemas/TokenFetchSchema";
import TokenSchema from "@schemas/TokenSchema";

export default async (router: FastifyInstance) => {
    router.addHook("preHandler", async (req, res, next) => {
        const { headers } = req;

        if (headers.authorization !== process.env.XANTHIC_KEY) return res.status(401).send({
            success: false,
            error: "unauthorized"
        });
    });

    router.patch("/link", {
        schema: { body: TokenFetchSchema }
    }, async (req, res) => {
        const { body, headers }: { body: any, headers: any } = req;

        const token = await Token.findOne({ token: body.token }, { relations: ["account"] });
        if (!token || !token.account || !token.account.enabled || token.type !== 0) return res.status(404).send({
            success: false,
            error: "invaild token"
        });

        if (token.account.discordId) return res.status(400).send({
            success: false,
            error: "account is already linked"
        });

        token.account.discordId = headers["x-discord-id"];
        await token.account.save();


        return {
            success: true,
            message: "linked account to your discord"
        };
    });


    router.post("/token/create", {
        preHandler: Xanthic,
        schema: {
            body: TokenSchema
        }
    }, async (req, res) => {
        const user: Account = req.account;
        const { body }: { body: any } = req;

        const { token } = await Token.generateToken({
            account: user,
            type: body.type
        })

        return {
            success: true,
            code: token
        };
    });

    router.get("/account/@me", {
        preHandler: Xanthic
    }, async (req, res) => {
        const user: Account = req.account;
        const { body }: { body: any } = req

        return {
            success: true,
            account: user.toObject()
        };
    })
};