import S from "fluent-json-schema"

export default S.object()
    .prop("id", S.string().required())