import S from "fluent-json-schema"

export default S.object()
    .prop("reason", S.string().raw({ nullable: true })).default(null)
    .prop("enabled", S.boolean().required());