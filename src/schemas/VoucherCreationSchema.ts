import S from "fluent-json-schema"

export default S.object()
    .prop("type", S.number().required())
    .prop("value", S.string().required())