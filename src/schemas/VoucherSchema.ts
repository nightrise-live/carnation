import S from "fluent-json-schema"

export default S.object()
    .prop("code", S.string().required());