import S from "fluent-json-schema"

export default S.object()
    .prop("k", S.string().required())