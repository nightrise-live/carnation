import S from "fluent-json-schema"
import CredentialSchema from "./CredentialSchema";

export default S.object()
    .prop("invite", S.string().required())
    .extend(CredentialSchema);
