import S from "fluent-json-schema"

export default S.object()
    .prop("password", S.string().required())