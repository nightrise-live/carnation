import S from "fluent-json-schema"

export default S.object()
    .prop("invite", S.number().required());