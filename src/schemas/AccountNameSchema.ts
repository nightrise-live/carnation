import S from "fluent-json-schema"

export default S.object()
    .prop("name", S.string().required())