import S from "fluent-json-schema"

export default S.object()
    .prop("message", S.string().required());