import S from "fluent-json-schema"

export default S.object()
    .prop("domain", S.string())
    .prop("subDomain", S.string())
    .prop("showUrl", S.boolean())
    .prop("fileNameLength", S.number().minimum(8))
    .prop("path", S.array().items(S.string()))
    .prop("noExtension", S.boolean())
    .additionalProperties(false)