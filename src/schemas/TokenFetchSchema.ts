import S from "fluent-json-schema"

export default S.object()
    .prop("token", S.string().required())