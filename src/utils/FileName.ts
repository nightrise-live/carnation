export default class FileName {
    name: string;
    ext: string;

    constructor(name: string, ext: string) {
        this.name = name;
        this.ext = ext;
    };

    getWith() {
        return this.name + this.ext;
    };

    getWithout() {
        return this.name;
    };
}