import { randomBytes } from "crypto"; 

export default (type: number) => {
    let lastSection = randomBytes(3);

    lastSection[2] = (lastSection[2] << 3) | type;

    return [
        randomBytes(3).toString("hex"),
        randomBytes(3).toString("hex"),
        randomBytes(3).toString("hex"),
        lastSection.toString("hex")
    ].join("-")
};