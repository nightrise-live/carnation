import { Account } from "@database/Account";

export default interface TokenOptions {
    type?: number;
    account: Account;
}