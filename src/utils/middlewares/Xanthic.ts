import { FastifyRequest, FastifyReply, HookHandlerDoneFunction } from "fastify"
import { Account } from "@database/Account";

export default async (req: FastifyRequest, res: FastifyReply, next: HookHandlerDoneFunction) => {
    const { headers }: { headers: any } = req;
    
    const account = await Account.findOne({ discordId: headers["x-discord-id"] });
    if (!account || !account.enabled) return res.status(404).send({
        success: false,
        error: "account was not found or not linked"
    });

    req.account = account;
};
