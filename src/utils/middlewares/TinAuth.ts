import { FastifyRequest, FastifyReply, HookHandlerDoneFunction } from "fastify"
import { Token } from "@database/Token";
import { TokenType } from "@utils/interfaces/TokenType";


export interface Options {
    type: TokenType
};

export default (options?: Options) => async (req: FastifyRequest, res: FastifyReply, next: HookHandlerDoneFunction) => {

    const token = await Token.findOne({
        token: req.headers.authorization || ""
    }, { relations: ["account"] });

    const type = options && options.type || TokenType.ACCOUNT;

    if (!token || !token.account.enabled || token.type !== type) return res.status(401).send({
        success: false,
        error: "unauthorized"
    });

    req.account = token.account;
};
