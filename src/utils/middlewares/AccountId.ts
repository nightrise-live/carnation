import { FastifyRequest, FastifyReply, HookHandlerDoneFunction } from "fastify"
import { Account } from "@database/Account";

const toNumber = (num: string) => {
    const number = parseInt(num);
    
    return !Number.isNaN(number) && number || -1;
};

export default async (req: FastifyRequest, res: FastifyReply, next: HookHandlerDoneFunction) => {
    const { params }: { params: any } = req;
    
    const account = await Account.findOne(toNumber(params.id));
    if (!account) return res.status(404).send({
        success: false,
        error: "account was not found"
    });

    req.account = account;
};
