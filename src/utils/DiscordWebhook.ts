import Axios from "axios";

export default (url: string, content: string) => {
    try {
        Axios.post(url, { content });
    } catch {}
};