import { readdir, stat } from "fs/promises";
import prettyBytes from "pretty-bytes";
import { join } from "path";

export default async (path: string): Promise<string> => {
    let size = 0;
    const files = await readdir(path);

    for (const i in files) {
        const stats = await stat(join(path, files[i]));

        size += stats.size;
    };

    return prettyBytes(size);
};