CREATE TABLE "account" (
    id SERIAL PRIMARY KEY,
    "name" text NOT NULL,
    "passwordHash" text NOT NULL,
    "invites" integer DEFAULT 0,
    "enabled" boolean DEFAULT true,
    "settings" text DEFAULT '{}',
    "reason" text,
    "discordId" text,
    "createdOn" timestamp DEFAULT NOW(),
    "storageUsed" double precision DEFAULT 0,
    "storageLimit" double precision DEFAULT 500
);

CREATE TABLE "token" (
    id SERIAL PRIMARY KEY,
    "token" text NOT NULL,
    "type" integer DEFAULT 0,
    "accountId" integer REFERENCES "account" (id) NOT NULL
);

CREATE TABLE "invite" (
    id SERIAL PRIMARY KEY,
    "code" text NOT NULL,
    "used" boolean DEFAULT false,
    "generatorId" integer REFERENCES "account" (id)
);


CREATE TABLE "file" (
    id integer GENERATED ALWAYS AS IDENTITY,
    "name" text NOT NULL,
    "path" text NOT NULL,
    "metaData" text DEFAULT '{}',
    "ContentType" text NOT NULL,
    "accessKey" text NOT NULL,
    "uploaderId" integer REFERENCES "account" (id),
    "accessible" boolean DEFAULT TRUE
);

CREATE TABLE "domain" (
    id SERIAL PRIMARY KEY,
    "name" text NOT NULL,
    "wildcard" boolean DEFAULT true
);

CREATE TABLE "voucher" (
    id SERIAL PRIMARY KEY,
    "type" integer NOT NULL,
    "code" text NOT NULL,
    "redeemed" boolean DEFAULT false,
    "value" text NOT NULL
);